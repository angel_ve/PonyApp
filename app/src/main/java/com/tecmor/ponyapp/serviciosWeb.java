package com.tecmor.ponyapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by angelve on 28/09/18.
 */

public class serviciosWeb {
    public final String ip="http://angelvepw.000webhostapp.com/WebServ/";
    //public final String ip="192.168.0.2/WebServ";
    HttpPost postHttp;
    HttpClient cliente;
    HttpResponse respuesta;
    HttpEntity entidad;
    Context contexto;

    serviciosWeb(Context context){
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        cliente = new DefaultHttpClient();
        contexto=context;
        Log.d("ServWeb","Constructor");
    }

    public boolean subirPubl(String objJson) throws IOException {
        JSONObject token= null;
        OkHttpClient cliente = new OkHttpClient();
        RequestBody formBody = null;
        try {
            token = new JSONObject(convertStreamToString(contexto.openFileInput("token.txt")));
            formBody=new FormBody.Builder().add("token",token.getString("codToken")).add("json",objJson).build();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Request request=new Request.Builder()
                .url(ip+"subirPost.php")
                .post(formBody)
                .build();
        Response respuesta=cliente.newCall(request).execute();
        String respString=respuesta.body().string();
        Log.d("ServWeb","Respuesta: \""+respString+"\", longitud: "+respString.length());
        if(respString.substring(0,2).equals("si")){
            respString=respString.substring(2,respString.length());
            SQLiteDatabase BDD = new SampleDBSQLiteHelper(contexto).getWritableDatabase();
            ContentValues valores = new ContentValues();
            JSONObject jsonRespuesta= null;
            try {
                jsonRespuesta = new JSONObject(respString);
                JSONObject jsonPar=new JSONObject(objJson);
                valores.put(DBContract.publicacion.COLUMN_USUARIO,jsonPar.getString(DBContract.publicacion.COLUMN_USUARIO));
                valores.put(DBContract.publicacion.COLUMN_CODIGO,jsonRespuesta.getString(DBContract.publicacion.COLUMN_CODIGO));
                valores.put(DBContract.publicacion.COLUMN_CONTENIDO,jsonPar.getString(DBContract.publicacion.COLUMN_CONTENIDO));
                valores.put(DBContract.publicacion.COLUMN_ACTIVO,"1");
                valores.put(DBContract.publicacion.COLUMN_CODIGO_GRUPO,jsonPar.getString(DBContract.publicacion.COLUMN_CODIGO_GRUPO));
                valores.put(DBContract.publicacion.COLUMN_FECHAPUBL,jsonRespuesta.getString(DBContract.publicacion.COLUMN_FECHAPUBL));
                valores.put(DBContract.publicacion.COLUMN_TITULO,jsonPar.getString(DBContract.publicacion.COLUMN_TITULO));
                if(BDD.insert(DBContract.publicacion.TABLE_NAME, null, valores)>=0)
                    return true;
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    public boolean subirComent(String objJson) throws IOException {
        JSONObject token= null;
        OkHttpClient cliente = new OkHttpClient();
        RequestBody formBody = null;
        try {
            token = new JSONObject(convertStreamToString(contexto.openFileInput("token.txt")));
            formBody=new FormBody.Builder().add("token",token.getString("codToken")).add("json",objJson).build();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Request request=new Request.Builder()
                .url(ip+"subirCom.php")
                .post(formBody)
                .build();
        Response respuesta=cliente.newCall(request).execute();
        String respString=respuesta.body().string();
        Log.d("ServWeb","Respuesta: \""+respString+"\", longitud: "+respString.length());
        if(respString.substring(0,2).equals("si")){
            respString=respString.substring(2,respString.length());
            SQLiteDatabase BDD = new SampleDBSQLiteHelper(contexto).getWritableDatabase();
            ContentValues valores = new ContentValues();
            JSONObject jsonRespuesta= null;
            try {
                jsonRespuesta = new JSONObject(respString);
                JSONObject jsonPar=new JSONObject(objJson);
                valores.put(DBContract.comentarios.COLUMN_CODIGO,jsonRespuesta.getString("codCom"));
                valores.put(DBContract.comentarios.COLUMN_FECHACOM,jsonRespuesta.getString("fechaCom"));
                valores.put(DBContract.comentarios.COLUMN_COMENTARIO, jsonPar.getString("comentario"));
                valores.put(DBContract.comentarios.COLUMN_CODPUBL,jsonPar.getString("codPubl"));
                valores.put(DBContract.comentarios.COLUMN_USUA, jsonPar.getString("usuario"));
                if(BDD.insert(DBContract.comentarios.TABLE_NAME, null, valores)>=0)
                    return true;
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }
        return false;
    }

    public boolean nComprobar(String numco, String passw) throws IOException {
        OkHttpClient cliente = new OkHttpClient();
        String json="{\"numCo\":\""+numco+"\",\"nip\":\""+passw+"\"}";
        RequestBody formBody = new FormBody.Builder()
                .add("json",json).build();
        Request request=new Request.Builder()
                .url(ip+"compr.php")
                .post(formBody)
                .build();
        Response respuesta=cliente.newCall(request).execute();
        if (!respuesta.isSuccessful()) throw new IOException("Código inesperado: " + respuesta);
        String respuestaString=respuesta.body().string();
        Log.d("Respuesta de Ok",respuestaString);
        SQLiteDatabase BDD = new SampleDBSQLiteHelper(contexto).getWritableDatabase();
        ContentValues valores = new ContentValues();
        JSONArray arrayResp;
        JSONObject jsonRespuesta;
        try {
            if(!respuestaString.substring(0, 2).equalsIgnoreCase("si")){
                return false;
            } else{
                respuestaString=respuestaString.substring(2);
                FileOutputStream outputStream = contexto.openFileOutput("token.txt", Context.MODE_PRIVATE);
                JSONObject objJson=new JSONObject(respuestaString);
                objJson.put("numCo",numco);
                objJson.put("pass",passw);
                outputStream.write(objJson.toString().getBytes());
                outputStream.close();
                Log.d("COMPR","EXITO");
                //HACER ESTO EN HILOS
                nConseguirFoto(numco);
                nConseguirDB(numco);
                nConseguirHorario(numco);
                nConseguirMat(numco);
                nConseguirCompaneros(numco);
                consegDepar();
                //conseguirComentarios
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean conseguirComGrupos(String numControl){
        return true;
    }

    public boolean nConseguirCompaneros(String numco) throws IOException {
        JSONObject token= null;
        OkHttpClient cliente = new OkHttpClient();
        RequestBody formBody = null;
        try {
            token = new JSONObject(convertStreamToString(contexto.openFileInput("token.txt")));
            formBody=new FormBody.Builder().add("token",token.getString("codToken")).add("numCo",numco).build();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Request request=new Request.Builder()
                .url(ip+"consultaCompaneros.php")
                .post(formBody)
                .build();
        Response respuesta=cliente.newCall(request).execute();
        if (!respuesta.isSuccessful()) throw new IOException("Código inesperado: " + respuesta);
        String respuestaString=respuesta.body().string();
        Log.d("Respuesta de Ok",respuestaString);
        SQLiteDatabase BDD = new SampleDBSQLiteHelper(contexto).getWritableDatabase();
        ContentValues valores = new ContentValues();
        JSONArray arrayResp;
        JSONObject jsonRespuesta;
        try {
            arrayResp=new JSONArray(respuestaString);
            Log.d("RESPUESTAWEB", String.valueOf(arrayResp.length()));
            for (int i = 0; i < arrayResp.length(); i++) {
                jsonRespuesta = new JSONObject(arrayResp.getJSONObject(i).toString());
                Log.d("INSERTANDO JSON", jsonRespuesta.toString());
                valores.put(DBContract.alumno.COLUMN_NODECONTROL, jsonRespuesta.getString(DBContract.alumno.COLUMN_NODECONTROL));
                valores.put(DBContract.alumno.COLUMN_NOMBRE_ALUMNO, jsonRespuesta.getString(DBContract.alumno.COLUMN_NOMBRE_ALUMNO));
                valores.put(DBContract.alumno.COLUMN_APELLIDO_PATERNO, jsonRespuesta.getString(DBContract.alumno.COLUMN_APELLIDO_PATERNO));
                valores.put(DBContract.alumno.COLUMN_APELLIDO_MATERNO, jsonRespuesta.getString(DBContract.alumno.COLUMN_APELLIDO_MATERNO));
                valores.put(DBContract.alumno.COLUMN_CARRERA, jsonRespuesta.getString(DBContract.alumno.COLUMN_CARRERA));
                valores.put(DBContract.alumno.COLUMN_ESPECIALIDAD, jsonRespuesta.getString(DBContract.alumno.COLUMN_ESPECIALIDAD));
                valores.put(DBContract.alumno.COLUMN_SEMESTRE, jsonRespuesta.getString(DBContract.alumno.COLUMN_SEMESTRE));
                long newRowId = BDD.insert(DBContract.alumno.TABLE_NAME, null, valores);
                Log.d("NEW ROW ID", String.valueOf(newRowId));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean nConseguirDB(String numco) throws IOException {
        JSONObject token= null;
        OkHttpClient cliente = new OkHttpClient();
        RequestBody formBody = null;
        try {
            token = new JSONObject(convertStreamToString(contexto.openFileInput("token.txt")));
            formBody=new FormBody.Builder().add("token",token.getString("codToken")).add("numCo",numco).build();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Request request=new Request.Builder()
                .url(ip+"conseguirDB.php")
                .post(formBody)
                .build();
        Response respuesta=cliente.newCall(request).execute();
        if (!respuesta.isSuccessful()) throw new IOException("Código inesperado: " + respuesta);
        String respuestaString=respuesta.body().string();
        Log.d("Respuesta de Ok",respuestaString);
        SQLiteDatabase BDD = new SampleDBSQLiteHelper(contexto).getWritableDatabase();
        ContentValues valores = new ContentValues();
        JSONArray arrayResp;
        JSONObject jsonRespuesta;
        try {
            arrayResp=new JSONArray(respuestaString);
            Log.d("RESPUESTAWEB", String.valueOf(arrayResp.length()));
            for (int i = 0; i < arrayResp.length(); i++) {
                jsonRespuesta = new JSONObject(arrayResp.getJSONObject(i).toString());
                Log.d("INSERTANDO JSON", jsonRespuesta.toString());
                valores.put(DBContract.publicacion.COLUMN_CODIGO, jsonRespuesta.getString(DBContract.publicacion.COLUMN_CODIGO));
                valores.put(DBContract.publicacion.COLUMN_ACTIVO, jsonRespuesta.getString(DBContract.publicacion.COLUMN_ACTIVO));
                valores.put(DBContract.publicacion.COLUMN_CODIGO_GRUPO, jsonRespuesta.getString(DBContract.publicacion.COLUMN_CODIGO_GRUPO));
                valores.put(DBContract.publicacion.COLUMN_FECHAPUBL, jsonRespuesta.getString(DBContract.publicacion.COLUMN_FECHAPUBL));
                valores.put(DBContract.publicacion.COLUMN_USUARIO, jsonRespuesta.getString(DBContract.publicacion.COLUMN_USUARIO));
                valores.put(DBContract.publicacion.COLUMN_TITULO, jsonRespuesta.getString(DBContract.publicacion.COLUMN_TITULO));
                valores.put(DBContract.publicacion.COLUMN_CONTENIDO, jsonRespuesta.getString(DBContract.publicacion.COLUMN_CONTENIDO));
                long newRowId = BDD.insert(DBContract.publicacion.TABLE_NAME, null, valores);
                Log.d("NEW ROW ID", String.valueOf(newRowId));
            }
            } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean nConseguirMat(String numco) throws IOException {
        JSONObject token= null;
        OkHttpClient cliente = new OkHttpClient();
        RequestBody formBody = null;
        try {
            token = new JSONObject(convertStreamToString(contexto.openFileInput("token.txt")));
            formBody=new FormBody.Builder().add("token",token.getString("codToken")).add("numCo",numco).build();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Request request=new Request.Builder()
                .url(ip+"matalum.php")
                .post(formBody)
                .build();
        Response respuesta=cliente.newCall(request).execute();
        if (!respuesta.isSuccessful()) throw new IOException("Código inesperado: " + respuesta);
        try {
            JSONArray arregJson=new JSONArray(respuesta.body().string());
            JSONObject objJson;
            ContentValues valores = new ContentValues();
            SQLiteDatabase BDD = new SampleDBSQLiteHelper(contexto).getWritableDatabase();
            for(int i=0;i<arregJson.length();i++){
                objJson=new JSONObject(arregJson.getJSONObject(i).toString());
                Log.d("INSERTANDO JSON", objJson.toString());
                valores.put(DBContract.materias.COLUMN_MATERIA, objJson.getString(DBContract.materias.COLUMN_MATERIA));
                valores.put(DBContract.materias.COLUMN_NOMBRE_ABREVIADO, objJson.getString(DBContract.materias.COLUMN_NOMBRE_ABREVIADO));
                valores.put(DBContract.materias.COLUMN_NOMBRE_COMPLETO, objJson.getString(DBContract.materias.COLUMN_NOMBRE_COMPLETO));
                long newRowId = BDD.insert(DBContract.materias.TABLE_NAME, null, valores);
                Log.d("NEW ROW ID", String.valueOf(newRowId));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean nConseguirHorario(String numco) throws IOException {
        JSONObject token= null;
        OkHttpClient cliente = new OkHttpClient();
        RequestBody formBody = null;
        try {
            token = new JSONObject(convertStreamToString(contexto.openFileInput("token.txt")));
            formBody=new FormBody.Builder().add("token",token.getString("codToken")).add("numCo",numco).build();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Request request=new Request.Builder()
                //.url(ip+"/horario.php")
                .url(ip+"horario.json")
                .post(formBody)
                .build();
        Response respuesta=cliente.newCall(request).execute();
        if (!respuesta.isSuccessful()){
            throw new IOException("Código inesperado: " + respuesta);
        } else {
            FileOutputStream outputStream = contexto.openFileOutput("horario.json", Context.MODE_PRIVATE);
            outputStream.write(respuesta.body().string().getBytes());
            outputStream.close();
            return true;
        }
    }

    public boolean nConseguirFoto(String numco) throws IOException {
        JSONObject token= null;
        OkHttpClient cliente = new OkHttpClient();
        RequestBody formBody = null;
        try {
            token = new JSONObject(convertStreamToString(contexto.openFileInput("token.txt")));
            formBody=new FormBody.Builder().add("token",token.getString("codToken")).add("numCo",numco).build();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Request request=new Request.Builder()
                //.url(ip+"horario.php")
                .url(ip+"foto.jpg")
                .post(formBody)
                .build();
        Response respuesta=cliente.newCall(request).execute();
        if (!respuesta.isSuccessful()){
            throw new IOException("Código inesperado: " + respuesta);
        } else {
            FileOutputStream outputStream = contexto.openFileOutput(numco+".jpg", Context.MODE_PRIVATE);
            outputStream.write(respuesta.body().string().getBytes());
            outputStream.close();
            return true;
        }
    }

    public boolean consultaPublGrupoOk(String codGrupo) throws IOException {
        JSONObject token= null;
        OkHttpClient cliente = new OkHttpClient();
        RequestBody formBody = null;
        try {
            token = new JSONObject(convertStreamToString(contexto.openFileInput("token.txt")));
            formBody=new FormBody.Builder().add("token",token.getString("codToken")).add("codGrupo",codGrupo).build();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Request request=new Request.Builder()
                .url(ip+"consultaPublGrupo.php")
                .post(formBody)
                .build();
        Response respuesta=cliente.newCall(request).execute();
        if (!respuesta.isSuccessful()){
            return false;
        }
        String respuestaString=respuesta.body().string();
        Log.d("Respuesta de Ok: ",respuestaString);
        SQLiteDatabase BDD = new SampleDBSQLiteHelper(contexto).getWritableDatabase();
        ContentValues valores = new ContentValues();
        JSONArray arrayResp;
        JSONObject jsonRespuesta;
        try {
            BDD.delete(DBContract.publicacion.TABLE_NAME, DBContract.publicacion.COLUMN_CODIGO_GRUPO + "=\"" + codGrupo + "\"", null);
            arrayResp = new JSONArray(respuestaString);
            Log.d("JSONARRAYOBJ", String.valueOf(arrayResp.length()));
            for (int i = 0; i < arrayResp.length(); i++) {
                jsonRespuesta = new JSONObject(arrayResp.getJSONObject(i).toString());
                Log.d("INSERTANDO JSON",jsonRespuesta.toString());
                valores.put(DBContract.publicacion.COLUMN_CODIGO, jsonRespuesta.getString("codigo"));
                valores.put(DBContract.publicacion.COLUMN_ACTIVO, jsonRespuesta.getString("valido"));
                valores.put(DBContract.publicacion.COLUMN_CODIGO_GRUPO, jsonRespuesta.getString("codiGrupo"));
                valores.put(DBContract.publicacion.COLUMN_FECHAPUBL, jsonRespuesta.getString("fechaPubl"));
                valores.put(DBContract.publicacion.COLUMN_USUARIO, jsonRespuesta.getString("usuario"));
                valores.put(DBContract.publicacion.COLUMN_TITULO, jsonRespuesta.getString("titulo"));
                valores.put(DBContract.publicacion.COLUMN_CONTENIDO, jsonRespuesta.getString("contenido"));
                long newRowId = BDD.insert(DBContract.publicacion.TABLE_NAME, null, valores);
                Log.d("NEW ROW ID", String.valueOf(newRowId));
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean consultaComenOk(String codPubl) throws IOException {
        JSONObject token= null;
        OkHttpClient cliente = new OkHttpClient();
        RequestBody formBody = null;
        try {
            token = new JSONObject(convertStreamToString(contexto.openFileInput("token.txt")));
            formBody=new FormBody.Builder().add("token",token.getString("codToken")).add("codPubl",codPubl).build();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Request request=new Request.Builder()
                .url(ip+"consultaComen.php")
                .post(formBody)
                .build();
        Response respuesta=cliente.newCall(request).execute();
        if (!respuesta.isSuccessful()) throw new IOException("Código inesperado: " + respuesta);
        String respuestaString=respuesta.body().string();
        Log.d("Respuesta de Ok: ",respuestaString);
        SQLiteDatabase BDD = new SampleDBSQLiteHelper(contexto).getWritableDatabase();
        ContentValues valores = new ContentValues();
        JSONArray arrayResp;
        JSONObject jsonRespuesta;
        try {
            BDD.delete(DBContract.comentarios.TABLE_NAME,DBContract.comentarios.COLUMN_CODIGO+"=\""+codPubl+"\"", null);
            arrayResp = new JSONArray(respuestaString);
            Log.d("JSONARRAYOBJ", String.valueOf(arrayResp.length()));
            for (int i = 0; i < arrayResp.length(); i++) {
                jsonRespuesta = new JSONObject(arrayResp.getJSONObject(i).toString());
                Log.d("INSERTANDO JSON",jsonRespuesta.toString());
                valores.put(DBContract.comentarios.COLUMN_CODIGO, jsonRespuesta.getString(DBContract.comentarios.COLUMN_CODIGO));
                valores.put(DBContract.comentarios.COLUMN_CODPUBL, jsonRespuesta.getString(DBContract.comentarios.COLUMN_CODPUBL));
                valores.put(DBContract.comentarios.COLUMN_ACTIVO, jsonRespuesta.getString(DBContract.comentarios.COLUMN_ACTIVO));
                valores.put(DBContract.comentarios.COLUMN_USUA, jsonRespuesta.getString(DBContract.comentarios.COLUMN_USUA));
                valores.put(DBContract.comentarios.COLUMN_FECHACOM, jsonRespuesta.getString(DBContract.comentarios.COLUMN_FECHACOM));
                valores.put(DBContract.comentarios.COLUMN_COMENTARIO, jsonRespuesta.getString(DBContract.comentarios.COLUMN_COMENTARIO));
                long newRowId = BDD.insert(DBContract.comentarios.TABLE_NAME, null, valores);
                Log.d("NEW ROW ID", String.valueOf(newRowId));
            }
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean verifConex(){
        OkHttpClient cliente = new OkHttpClient();
        Request request=new Request.Builder()
                .url(ip+"index.php")
                .build();
        try {
            Response respuesta= null;
            respuesta = cliente.newCall(request).execute();
            int codRes=respuesta.code();
            Log.d("SERVWEB","Codigo de resp: "+codRes);
            if(codRes==200){
                return true;
            } else{
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean eliminarCom(String codCom) throws IOException {
        JSONObject token= null;
        OkHttpClient cliente = new OkHttpClient();
        RequestBody formBody = null;
        try {
            token = new JSONObject(convertStreamToString(contexto.openFileInput("token.txt")));
            formBody=new FormBody.Builder().add("token",token.getString("codToken")).add("codCom",codCom).build();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Request request=new Request.Builder()
                .url(ip+"elimCom.php")
                .post(formBody)
                .build();
        Response respuesta=cliente.newCall(request).execute();
        SQLiteDatabase BDD = new SampleDBSQLiteHelper(contexto).getWritableDatabase();
        ContentValues valores=new ContentValues();
        String stringResp=respuesta.body().string();
        Log.d("Respuesta elim", stringResp);
            if(stringResp.substring(0,2).equalsIgnoreCase("si")){
                valores.put(DBContract.comentarios.COLUMN_ACTIVO,0);
                BDD.update(DBContract.comentarios.TABLE_NAME,valores,DBContract.comentarios.COLUMN_CODIGO+"="+codCom,null);
                return true;
            }else{
                return false;
            }
    }


    public boolean eliminarPub(String codPubl) throws IOException {
        JSONObject token= null;
        OkHttpClient cliente = new OkHttpClient();
        RequestBody formBody = null;
        try {
            token = new JSONObject(convertStreamToString(contexto.openFileInput("token.txt")));
            formBody=new FormBody.Builder().add("token",token.getString("codToken")).add("codPubl",codPubl).build();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Request request=new Request.Builder()
                .url(ip+"elimPubl.php")
                .post(formBody)
                .build();
        Response respuesta=cliente.newCall(request).execute();
        SQLiteDatabase BDD = new SampleDBSQLiteHelper(contexto).getWritableDatabase();
        ContentValues valores=new ContentValues();
        String stringResp=respuesta.body().string();
        Log.d("Respuesta elim", stringResp);
        if(stringResp.substring(0,2).equalsIgnoreCase("si")){
            valores.put(DBContract.publicacion.COLUMN_ACTIVO,0);
            BDD.update(DBContract.publicacion.TABLE_NAME,valores,DBContract.publicacion.COLUMN_CODIGO+"="+codPubl,null);
            return true;
        }else{
            return false;
        }
    }

    public boolean consegDepar() throws IOException {
        OkHttpClient cliente = new OkHttpClient();
        Request request=new Request.Builder()
                .url(ip+"consDepar.php")
                .build();
        Response respuesta=cliente.newCall(request).execute();
        if (!respuesta.isSuccessful()) throw new IOException("Código inesperado: " + respuesta);
        String respuestaString=respuesta.body().string();
        Log.d("Respuesta de Ok: ",respuestaString);
        //SQLiteDatabase BDD = new SampleDBSQLiteHelper(contexto).getWritableDatabase();
        //ContentValues valores = new ContentValues();
        JSONArray arrayResp;
        JSONObject jsonRespuesta;
        return false;
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try
        {
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            is.close();
        }
        catch (IOException e)
        { }
        return sb.toString();
    }

}
