package com.tecmor.ponyapp;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class fotoGrande extends Fragment {

    String numCo;

    public fotoGrande() {
        // Required empty public constructor
    }

    public static fotoGrande newInstance(String param1, String param2) {
        fotoGrande fragment = new fotoGrande();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //super.onCreate(savedInstanceState);
        return inflater.inflate(R.layout.fragment_foto_grande, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageView imagenFoto = (ImageView) getView().findViewById(R.id.imageView5);
        numCo=this.getArguments().getString("numCo");
        File fileFoto = new File(getActivity().getFilesDir().getPath()+'/'+numCo+".jpg");
        Bitmap bitmap = BitmapFactory.decodeFile(fileFoto.getAbsolutePath());
        Log.d("ARCHIVOS","Ubicación :"+fileFoto.getAbsolutePath()+fileFoto.getTotalSpace());
        //Log.d("ARCHIVO", fileFoto.getAbsolutePath());
        //Picasso.with(this).load(fileFoto).fit().centerCrop().into(foto);
        //imagenFoto.setImageURI(Uri.fromFile(fileFoto));
        imagenFoto.setImageBitmap(bitmap);
        getActivity().setTitle("Foto");
        /*
        MenuItem actSet=(MenuItem) getView().findViewById(R.id.action_settings);
        //actSet.
        //actSet.setTitle("Descargar imagen");
        actSet.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                DownloadManager refDes = (DownloadManager)getActivity().getSystemService(Context.DOWNLOAD_SERVICE);
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse("https://sii.itmorelia.edu.mx/storage/data/alumnos/" + getArguments().getString("numco") + "/foto.jpg"));
                request.setTitle("Descarga de foto");
                request.setDestinationInExternalFilesDir(getActivity(), Environment.DIRECTORY_DOWNLOADS,"foto.jpg");
                refDes.enqueue(request);
                return false;
            }
        });*/

    }
}
