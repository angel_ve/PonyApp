package com.tecmor.ponyapp;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;


public class fragmentHorarioFlechas extends Fragment {
    Calendar calendario=Calendar.getInstance();
    int diaActual=calendario.get(Calendar.DAY_OF_WEEK);


    public fragmentHorarioFlechas() {
        // Required empty public constructor
    }


    public static fragmentHorarioFlechas newInstance(String param1, String param2) {
        fragmentHorarioFlechas fragment = new fragmentHorarioFlechas();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Horario");
        ImageButton btnAnt=(ImageButton) getView().findViewById(R.id.imageButton);
        ImageButton btnSig=(ImageButton) getView().findViewById(R.id.imageButton2);
        Log.d("DIA", String.valueOf(diaActual));
        if(diaActual==1)
            diaActual=2;
        else if(diaActual==7)
            diaActual=2;
        cargar(diaActual);
        cargar(diaActual);
        final FloatingActionButton flotaBtn=(FloatingActionButton) getView().getRootView().findViewById(R.id.fab);
        flotaBtn.setVisibility(View.GONE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        return inflater.inflate(R.layout.fragment_horario_flechas, container, false);
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try
        {
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            is.close();
        }
        catch (IOException e)
        { }
        return sb.toString();
    }

    private void cargar(int diaC) {
        //setContentView(R.layout.activity_main);
        TextView dia = (TextView) getView().findViewById(R.id.textDia);
        ImageButton btnAnt = (ImageButton) getView().findViewById(R.id.imageButton);
        ImageButton btnSig = (ImageButton) getView().findViewById(R.id.imageButton2);
        btnAnt.setVisibility(View.VISIBLE);
        btnSig.setVisibility(View.VISIBLE);
        JSONArray json = null;
        JSONObject jsonDia = null;
        TextView nomMat1 = (TextView) getView().findViewById(R.id.NomMat1);
        TextView nomMat2 = (TextView) getView().findViewById(R.id.NomMat2);
        TextView nomMat3 = (TextView) getView().findViewById(R.id.NomMat3);
        TextView nomMat4 = (TextView) getView().findViewById(R.id.NomMat4);
        TextView nomMat5 = (TextView) getView().findViewById(R.id.NomMat5);
        TextView nomMat6 = (TextView) getView().findViewById(R.id.NomMat6);
        TextView nomMat7 = (TextView) getView().findViewById(R.id.NomMat7);
        TextView nomMat8 = (TextView) getView().findViewById(R.id.NomMat8);
        TextView nomProf1 = (TextView) getView().findViewById(R.id.NomProf1);
        TextView nomProf2 = (TextView) getView().findViewById(R.id.NomProf2);
        TextView nomProf3 = (TextView) getView().findViewById(R.id.NomProf3);
        TextView nomProf4 = (TextView) getView().findViewById(R.id.NomProf4);
        TextView nomProf5 = (TextView) getView().findViewById(R.id.NomProf5);
        TextView nomProf6 = (TextView) getView().findViewById(R.id.NomProf6);
        TextView nomProf7 = (TextView) getView().findViewById(R.id.NomProf7);
        TextView nomProf8 = (TextView) getView().findViewById(R.id.NomProf8);
        TextView hora1 = (TextView) getView().findViewById(R.id.Hora1);
        TextView hora2 = (TextView) getView().findViewById(R.id.Hora2);
        TextView hora3 = (TextView) getView().findViewById(R.id.Hora3);
        TextView hora4 = (TextView) getView().findViewById(R.id.Hora4);
        TextView hora5 = (TextView) getView().findViewById(R.id.Hora5);
        TextView hora6 = (TextView) getView().findViewById(R.id.Hora6);
        TextView hora7 = (TextView) getView().findViewById(R.id.Hora7);
        TextView hora8 = (TextView) getView().findViewById(R.id.Hora8);
        TextView salon1 = (TextView) getView().findViewById(R.id.Salon1);
        TextView salon2 = (TextView) getView().findViewById(R.id.Salon2);
        TextView salon3 = (TextView) getView().findViewById(R.id.Salon3);
        TextView salon4 = (TextView) getView().findViewById(R.id.Salon4);
        TextView salon5 = (TextView) getView().findViewById(R.id.Salon5);
        TextView salon6 = (TextView) getView().findViewById(R.id.Salon6);
        TextView salon7 = (TextView) getView().findViewById(R.id.Salon7);
        TextView salon8 = (TextView) getView().findViewById(R.id.Salon8);
        int longitud = 0;
        JSONArray mat = null;
        try {
            json = new JSONArray(convertStreamToString(getActivity().openFileInput("horario.json")));
            //Log.d("JSON",String.valueOf(json.getString(diaActual-2)));
            jsonDia = new JSONObject(json.getString(diaActual - 2));
            longitud = jsonDia.getJSONArray("DIA").length();
            mat = jsonDia.getJSONArray("DIA");
            //Log.d("JSON", mat1.getString("NombreMAte").toString());
            JSONObject[] materias = new JSONObject[longitud];
            for (int i = 0; i < longitud; i++) {
                materias[i] = new JSONObject(mat.getJSONObject(i).toString());
            }
            switch (longitud) {
                case 8:
                    nomMat8.setText(materias[7].getString("NombreMAte").toString());
                    nomProf8.setText(materias[7].getString("NombreProfe").toString());
                    hora8.setText(materias[7].getString("HoraIni").toString() + "-" + materias[7].getString("HoraFin").toString());
                    salon8.setText(materias[7].getString("Salon").toString());
                    nomMat8.setVisibility(View.VISIBLE);
                    nomProf8.setVisibility(View.VISIBLE);
                    hora8.setVisibility(View.VISIBLE);
                    salon8.setVisibility(View.VISIBLE);
                case 7:
                    nomMat7.setText(materias[6].getString("NombreMAte").toString());
                    nomProf7.setText(materias[6].getString("NombreProfe").toString());
                    hora7.setText(materias[6].getString("HoraIni").toString() + "-" + materias[6].getString("HoraFin").toString());
                    salon7.setText(materias[6].getString("Salon").toString());
                    nomMat7.setVisibility(View.VISIBLE);
                    nomProf7.setVisibility(View.VISIBLE);
                    hora7.setVisibility(View.VISIBLE);
                    salon7.setVisibility(View.VISIBLE);
                case 6:
                    nomMat6.setText(materias[5].getString("NombreMAte").toString());
                    nomProf6.setText(materias[5].getString("NombreProfe").toString());
                    hora6.setText(materias[5].getString("HoraIni").toString() + "-" + materias[5].getString("HoraFin").toString());
                    salon6.setText(materias[5].getString("Salon").toString());
                    nomMat6.setVisibility(View.VISIBLE);
                    nomProf6.setVisibility(View.VISIBLE);
                    hora6.setVisibility(View.VISIBLE);
                    salon6.setVisibility(View.VISIBLE);
                case 5:
                    nomMat5.setText(materias[4].getString("NombreMAte").toString());
                    nomProf5.setText(materias[4].getString("NombreProfe").toString());
                    hora5.setText(materias[4].getString("HoraIni").toString() + "-" + materias[4].getString("HoraFin").toString());
                    salon5.setText(materias[4].getString("Salon").toString());
                    nomMat5.setVisibility(View.VISIBLE);
                    nomProf5.setVisibility(View.VISIBLE);
                    hora5.setVisibility(View.VISIBLE);
                    salon5.setVisibility(View.VISIBLE);
                case 4:
                    nomMat4.setText(materias[3].getString("NombreMAte").toString());
                    nomProf4.setText(materias[3].getString("NombreProfe").toString());
                    hora4.setText(materias[3].getString("HoraIni").toString() + "-" + materias[3].getString("HoraFin").toString());
                    salon4.setText(materias[3].getString("Salon").toString());
                    nomMat8.setVisibility(View.VISIBLE);
                    nomProf8.setVisibility(View.VISIBLE);
                    hora8.setVisibility(View.VISIBLE);
                    salon8.setVisibility(View.VISIBLE);
                case 3:
                    nomMat3.setText(materias[2].getString("NombreMAte").toString());
                    nomProf3.setText(materias[2].getString("NombreProfe").toString());
                    hora3.setText(materias[2].getString("HoraIni").toString() + "-" + materias[2].getString("HoraFin").toString());
                    salon3.setText(materias[2].getString("Salon").toString());
                    nomMat3.setVisibility(View.VISIBLE);
                    nomProf3.setVisibility(View.VISIBLE);
                    hora3.setVisibility(View.VISIBLE);
                    salon3.setVisibility(View.VISIBLE);
                case 2:
                    nomMat2.setText(materias[1].getString("NombreMAte").toString());
                    nomProf2.setText(materias[1].getString("NombreProfe").toString());
                    hora2.setText(materias[1].getString("HoraIni").toString() + "-" + materias[1].getString("HoraFin").toString());
                    salon2.setText(materias[1].getString("Salon").toString());
                    nomMat2.setVisibility(View.VISIBLE);
                    nomProf2.setVisibility(View.VISIBLE);
                    hora2.setVisibility(View.VISIBLE);
                    salon2.setVisibility(View.VISIBLE);
                case 1:
                    nomMat1.setText(materias[0].getString("NombreMAte").toString());
                    nomProf1.setText(materias[0].getString("NombreProfe").toString());
                    hora1.setText(materias[0].getString("HoraIni").toString() + "-" + materias[0].getString("HoraFin").toString());
                    salon1.setText(materias[0].getString("Salon").toString());
                    nomMat1.setVisibility(View.VISIBLE);
                    nomProf1.setVisibility(View.VISIBLE);
                    hora1.setVisibility(View.VISIBLE);
                    salon1.setVisibility(View.VISIBLE);
                case 0:
                    break;
            }
            switch (longitud) {
                case 0:
                    nomMat1.setVisibility(View.INVISIBLE);
                    nomProf1.setVisibility(View.INVISIBLE);
                    hora1.setVisibility(View.INVISIBLE);
                    salon1.setVisibility(View.INVISIBLE);
                case 1:
                    nomMat2.setVisibility(View.INVISIBLE);
                    nomProf2.setVisibility(View.INVISIBLE);
                    hora2.setVisibility(View.INVISIBLE);
                    salon2.setVisibility(View.INVISIBLE);
                case 2:
                    nomMat3.setVisibility(View.INVISIBLE);
                    nomProf3.setVisibility(View.INVISIBLE);
                    hora3.setVisibility(View.INVISIBLE);
                    salon3.setVisibility(View.INVISIBLE);
                case 3:
                    nomMat4.setVisibility(View.INVISIBLE);
                    nomProf4.setVisibility(View.INVISIBLE);
                    hora4.setVisibility(View.INVISIBLE);
                    salon4.setVisibility(View.INVISIBLE);
                case 4:
                    nomMat5.setVisibility(View.INVISIBLE);
                    nomProf5.setVisibility(View.INVISIBLE);
                    hora5.setVisibility(View.INVISIBLE);
                    salon5.setVisibility(View.INVISIBLE);
                case 5:
                    nomMat6.setVisibility(View.INVISIBLE);
                    nomProf6.setVisibility(View.INVISIBLE);
                    hora6.setVisibility(View.INVISIBLE);
                    salon6.setVisibility(View.INVISIBLE);
                case 6:
                    nomMat7.setVisibility(View.INVISIBLE);
                    nomProf7.setVisibility(View.INVISIBLE);
                    hora7.setVisibility(View.INVISIBLE);
                    salon7.setVisibility(View.INVISIBLE);
                case 7:
                    nomMat8.setVisibility(View.INVISIBLE);
                    nomProf8.setVisibility(View.INVISIBLE);
                    hora8.setVisibility(View.INVISIBLE);
                    salon8.setVisibility(View.INVISIBLE);
                case 8:
                    break;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        switch (diaC) {
            case 1:
            case 2:
                dia.setText("Lunes");
                btnAnt.setVisibility(View.INVISIBLE);
                break;
            case 3:
                dia.setText("Martes");
                break;
            case 4:
                dia.setText("Miérc.");
                break;
            case 5:
                dia.setText("Jueves");
                break;
            case 6:
                dia.setText("Viernes");
                btnSig.setVisibility(View.INVISIBLE);
                break;
            case 7:
                dia.setText("Lunes");
                btnAnt.setVisibility(View.INVISIBLE);
                break;
        }
        btnAnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                diaActual--;
                cargar(diaActual);
            }
        });

        btnSig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                diaActual++;
                cargar(diaActual);
            }
        });
    }
}
