package com.tecmor.ponyapp;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

public class infoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        TextView info=(TextView) findViewById(R.id.textViewInfo1);
        info.setText(info.getText()+BuildConfig.VERSION_NAME);
        TextView feedback = (TextView) findViewById(R.id.textViewSug);
        feedback.setText(Html.fromHtml("<a href=\"mailto:angviesc@gmail.com?subject%20Sugerencia de PonyApp\">Manda sugerencias</a>"));
        feedback.setMovementMethod(LinkMovementMethod.getInstance());
    }
}
