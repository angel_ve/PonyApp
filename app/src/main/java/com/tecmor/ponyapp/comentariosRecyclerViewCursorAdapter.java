package com.tecmor.ponyapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tecmor.ponyapp.databinding.ComentarioListItemBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class comentariosRecyclerViewCursorAdapter extends RecyclerView.Adapter<comentariosRecyclerViewCursorAdapter.ViewHolder> {

    Context mContext;
    Cursor mCursor;
    JSONObject mjson;

    public comentariosRecyclerViewCursorAdapter(Context context, Cursor cursor, JSONObject OBJson) {
        mContext=context;
        mCursor=cursor;
        mjson=OBJson;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        ComentarioListItemBinding itemBinding;

        public ViewHolder (View itemView){
            super(itemView);
            itemBinding=DataBindingUtil.bind(itemView);
        }

        public void bindCursor(Cursor cursor){
            String nombre=cursor.getString(cursor.getColumnIndexOrThrow(DBContract.alumno.COLUMN_NOMBRE_ALUMNO))+" "+cursor.getString(cursor.getColumnIndexOrThrow(DBContract.alumno.COLUMN_APELLIDO_PATERNO))+" "+cursor.getString(cursor.getColumnIndexOrThrow(DBContract.alumno.COLUMN_APELLIDO_MATERNO));
            itemBinding.NombreLabel.setText(nombre);
            itemBinding.FechaLabel.setText(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.comentarios.COLUMN_FECHACOM)));
            itemBinding.comentLabel.setText(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.comentarios.COLUMN_COMENTARIO)));
            itemBinding.txtCodCom.setText(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.comentarios.COLUMN_CODIGO)));
            itemBinding.txtCodUsu.setText(cursor.getString(cursor.getColumnIndexOrThrow(DBContract.comentarios.COLUMN_USUA)));
            Log.d("Cadena",itemBinding.txtCodCom.getText().toString());
            /*itemBinding.FotoImage.setImageURI(Uri.parse(cursor.getString(
                    cursor.getColumnIndexOrThrow(DBContract.publicacion.COLUMN_USUARIO_FOTO)*/
            //itemBinding.FotoImage.setImageResource(R.drawable.ic_person_black_24dp);
        }
    }

    @Override
    public int getItemCount(){
        return mCursor.getCount();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        mCursor.moveToPosition(position);
        holder.bindCursor(mCursor);
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                CharSequence[] items=new CharSequence[1];
//                final CharSequence[] items = new CharSequence[3];
                try {
                    if(holder.itemBinding.txtCodUsu.getText().toString().equals(mjson.getString("numco"))){
                        items = new CharSequence[2];
                        items[0] = "Copiar texto";
                        /*items[1] = "Modificar"*/
                        items[1] = "Eliminar";
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                builder.setItems(items, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case 0:
                                        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                                        //ClipData clip = ClipData.newPlainText("Publicación",holder.mContView.getText());
                                        ClipData clip = ClipData.newPlainText("Publicación",holder.itemBinding.comentLabel.getText().toString());
                                        clipboard.setPrimaryClip(clip);
                                        break;
                                    case 1:
                                        //Conseguir id del comentario y eliminarlo lógicamente
                                        String codCom=holder.itemBinding.txtCodCom.getText().toString();
                                        serviciosWeb sw=new serviciosWeb(mContext);
                                        try {
                                            if(sw.eliminarCom(codCom)){
                                                Toast.makeText(mContext,"Comentario eliminado",Toast.LENGTH_SHORT).show();
                                                ((Activity)mContext).finish();
                                            } else{
                                                Toast.makeText(mContext,"Comentario no eliminado",Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (IOException e) {
                                            Toast.makeText(mContext,"Comentario no eliminado",Toast.LENGTH_SHORT).show();
                                            e.printStackTrace();
                                        }
                                        break;
                                }
                            }
                        });
                builder.show();
                return false;
            }
        });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.comentario_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }
}
