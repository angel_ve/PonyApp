package com.tecmor.ponyapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.tecmor.ponyapp.databinding.GrupoListItemBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by angelve on 19/09/18.
 */

public class publicacionRecyclerViewCursorAdapter extends RecyclerView.Adapter<publicacionRecyclerViewCursorAdapter.ViewHolder> {

    Context mContext;
    Cursor mCursor;
    JSONObject objson;

    public publicacionRecyclerViewCursorAdapter(Context context, Cursor cursor, JSONObject objJson) {
        mContext=context;
        mCursor=cursor;
        objson=objJson;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        GrupoListItemBinding itemBinding;

        public ViewHolder (View itemView){
            super(itemView);
            itemBinding=DataBindingUtil.bind(itemView);
        }

        public void bindCursor(Cursor cursor){
            itemBinding.DescripLabel.setText(cursor.getString(
                    cursor.getColumnIndexOrThrow(DBContract.publicacion.COLUMN_CONTENIDO)));
            itemBinding.FechaLabel.setText(cursor.getString(
                    cursor.getColumnIndexOrThrow(DBContract.publicacion.COLUMN_FECHAPUBL)));
//            itemBinding.NombreLabel.setText(cursor.getString(
                    //cursor.getColumnIndexOrThrow(DBContract.publicacion.COLUMN_USUARIO)));
            String nombre=cursor.getString(cursor.getColumnIndexOrThrow(DBContract.alumno.COLUMN_NOMBRE_ALUMNO))+" "+cursor.getString(cursor.getColumnIndexOrThrow(DBContract.alumno.COLUMN_APELLIDO_PATERNO))+" "+cursor.getString(cursor.getColumnIndexOrThrow(DBContract.alumno.COLUMN_APELLIDO_MATERNO));
            itemBinding.NombreLabel.setText(nombre);
            itemBinding.TituloLabel.setText(cursor.getString(
                    cursor.getColumnIndexOrThrow(DBContract.publicacion.COLUMN_TITULO)));
            itemBinding.txtCodPubl.setText(cursor.getString(
                    cursor.getColumnIndexOrThrow(DBContract.publicacion.COLUMN_CODIGO)));
            itemBinding.txtCodUsu.setText(cursor.getString(
                    cursor.getColumnIndexOrThrow(DBContract.publicacion.COLUMN_USUARIO)));
            Log.d("Cadena",itemBinding.txtCodPubl.getText().toString());
            /*itemBinding.FotoImage.setImageURI(Uri.parse(cursor.getString(
                    cursor.getColumnIndexOrThrow(DBContract.publicacion.COLUMN_USUARIO_FOTO)*/
            //itemBinding.FotoImage.setImageResource(R.drawable.ic_person_black_24dp);
        }
    }

    @Override
    public int getItemCount(){
        int conteo=mCursor.getCount();
        Log.d("Conteo: ", String.valueOf(conteo));
        //mCursor.close();
        return conteo;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        mCursor.moveToPosition(position);
        holder.bindCursor(mCursor);
        if (holder.itemBinding.DescripLabel.getLineCount() > 4 && holder.itemBinding.DescripLabel.getText().length() > 4) {
            int contText = holder.itemBinding.DescripLabel.getText().toString().length() / holder.itemBinding.DescripLabel.getLineCount();
            holder.itemBinding.DescripLabel.setText(holder.itemBinding.DescripLabel.getText().toString().subSequence(0, (contText * 4) - 3) + " ...Abrir publicación");
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intento=new Intent(mContext.getApplicationContext(),verpubliActivity.class);
                Intent intento=new Intent(mContext,com.tecmor.ponyapp.verpubliActivity.class);
                Log.d("Cadena publ:",holder.itemBinding.txtCodPubl.getText().toString());
                intento.putExtra("codPubl",holder.itemBinding.txtCodPubl.getText().toString());
//                intento.putExtra("codUsua",);
                try {
                    intento.putExtra("codUsua",objson.getString("numco"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mContext.startActivity(intento);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                CharSequence[] items=new CharSequence[1];
//                final CharSequence[] items = new CharSequence[3];
                try {
                    if(holder.itemBinding.txtCodUsu.getText().toString().equals(objson.getString("numco"))){
                        items = new CharSequence[2];
                        items[0] = "Copiar texto";
                        /*items[1] = "Modificar"*/
                        items[1] = "Eliminar";
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                //builder.setTitle("Publicación")
                builder.setItems(items, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        ClipboardManager clipboard = (ClipboardManager) mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                                        ClipData clip = ClipData.newPlainText("Publicación", holder.itemBinding.DescripLabel.getText().toString());
                                        clipboard.setPrimaryClip(clip);
                                        break;
                                    case 1:
                                        String codPubl = holder.itemBinding.txtCodPubl.getText().toString();
                                        serviciosWeb sw = new serviciosWeb(mContext);
                                        try {
                                            if (sw.eliminarPub(codPubl)) {
                                                Toast.makeText(mContext, "Publicación eliminada", Toast.LENGTH_SHORT).show();
                                                ((Activity) mContext).finish();
                                            } else {
                                                Toast.makeText(mContext, "Publicación no eliminada", Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (IOException e) {
                                            Toast.makeText(mContext, "Publicación no eliminada", Toast.LENGTH_SHORT).show();
                                            e.printStackTrace();
                                        }
                                        break;
                                }
                            }
                        });
                builder.show();
                return false;
            }
        });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.grupo_list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        viewHolder.itemBinding.imgSwitch.setFactory(new ViewSwitcher.ViewFactory() {
            public View makeView() {
                ImageView myView = new ImageView(mContext);
                return myView;
            }
        });
        Animation in = AnimationUtils.loadAnimation(mContext,android.R.anim.slide_in_left);
        Animation out = AnimationUtils.loadAnimation(mContext,android.R.anim.slide_out_right);
        viewHolder.itemBinding.imgSwitch.setImageResource(R.mipmap.pony_grande);
        viewHolder.itemBinding.imgSwitch.setInAnimation(in);
        viewHolder.itemBinding.imgSwitch.setOutAnimation(out);
        return viewHolder;
    }
}
