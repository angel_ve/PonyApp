package com.tecmor.ponyapp;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLClientInfoException;
//import com.tecmor.ponyapp.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    String numco;
    JSONArray mat;
    Cursor cursor;
    //private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //binding = DataBindingUtil.setContentView(this,R.layout.activity_main);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        displaySelectedScreen(R.layout.fragment_noticias);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent nuevaPub=new Intent(view.getContext(),publicacion.class);
                startActivity(nuevaPub);
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();**/
            }
        });

        //final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_main);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View hv=navigationView.getHeaderView(0);
        TextView tvNum = (TextView)hv.findViewById(R.id.numEst);
        TextView tvNombre=(TextView)hv.findViewById(R.id.nombreEst);
        ImageView foto=(ImageView) hv.findViewById(R.id.ivFoto);
        numco=getIntent().getStringExtra("numCo");
        tvNum.setText(numco);
        tvNombre.setText(getIntent().getStringExtra("nombre"));
        Menu menuNav=navigationView.getMenu();
        //código horrible a continuación
        MenuItem itemGrupo1=menuNav.findItem(R.id.nav_grupo_1);
        MenuItem itemGrupo2=menuNav.findItem(R.id.nav_grupo_2);
        MenuItem itemGrupo3=menuNav.findItem(R.id.nav_grupo_3);
        MenuItem itemGrupo4=menuNav.findItem(R.id.nav_grupo_4);
        MenuItem itemGrupo5=menuNav.findItem(R.id.nav_grupo_5);
        MenuItem itemGrupo6=menuNav.findItem(R.id.nav_grupo_6);
        MenuItem itemGrupo7=menuNav.findItem(R.id.nav_grupo_7);
        MenuItem itemGrupo8=menuNav.findItem(R.id.nav_grupo_8);
        itemGrupo1.setVisible(false);
        itemGrupo2.setVisible(false);
        itemGrupo3.setVisible(false);
        itemGrupo4.setVisible(false);
        itemGrupo5.setVisible(false);
        itemGrupo6.setVisible(false);
        itemGrupo7.setVisible(false);
        itemGrupo8.setVisible(false);
        //int cantMat= json get materias.txt
        int longitud = 0;
        JSONObject matO=null;
        SQLiteDatabase BDD = new SampleDBSQLiteHelper(this.getApplicationContext()).getWritableDatabase();
        cursor=BDD.rawQuery(DBContract.SELECT_FROM_MATERIAS,null);
        cursor.moveToFirst();
        for(int i=0;i<cursor.getCount();i++){
            Log.d("Cursor",cursor.getString(1));
            cursor.moveToNext();
        }
            switch (cursor.getCount()) {
                case 8:
                    itemGrupo8.setVisible(true);
                    cursor.moveToPosition(7);
                    itemGrupo8.setTitle(capitalizar(cursor.getString(2)));
                case 7:
                    itemGrupo7.setVisible(true);
                    cursor.moveToPosition(6);
                    itemGrupo7.setTitle(capitalizar(cursor.getString(2)));
                case 6:
                    itemGrupo6.setVisible(true);
                    cursor.moveToPosition(5);
                    itemGrupo6.setTitle(capitalizar(cursor.getString(2)));
                case 5:
                    itemGrupo5.setVisible(true);
                    cursor.moveToPosition(4);
                    itemGrupo5.setTitle(capitalizar(cursor.getString(2)));
                case 4:
                    itemGrupo4.setVisible(true);
                    cursor.moveToPosition(3);
                    itemGrupo4.setTitle(capitalizar(cursor.getString(2)));
                case 3:
                    itemGrupo3.setVisible(true);
                    cursor.moveToPosition(2);
                    itemGrupo3.setTitle(capitalizar(cursor.getString(2)));
                case 2:
                    itemGrupo2.setVisible(true);
                    cursor.moveToPosition(1);
                    itemGrupo2.setTitle(capitalizar(cursor.getString(2)));
                case 1:
                    itemGrupo1.setVisible(true);
                    cursor.moveToPosition(0);
                    itemGrupo1.setTitle(capitalizar(cursor.getString(2)));
                    break;
                default:
                    break;
            }

        //Abrir imagen                                                                                                                                                                                                                                                                                                                                                   //foto.setImageURI(Uri.fromFile(fileFoto));
        try {
            //FileInputStream FileFoto = openFileInput("foto.jpg");
            FileInputStream FileFoto = openFileInput(numco+".jpg");
            Bitmap BitmapFoto = BitmapFactory.decodeStream(FileFoto);
            foto.setImageBitmap(BitmapFoto);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        foto.setOnTouchListener(new View.OnTouchListener()
        {

            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                displaySelectedScreen(R.id.ivFoto);
                return false;
            }
        });
        displaySelectedScreen(R.id.nav_noticias);
    }


    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try
        {
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
            is.close();
        }
        catch (IOException e)
        { }
        return sb.toString();
    }

    @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
        //finish();
        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            //finish();
            //System.exit(0);
            //drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        //menu.findItem(R.id.nav_grupo_1).setTitle("HOLA");
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu){
        //MenuItem grupo1=menu.findItem(R.id.nav_grupo_1);
        //menu.findItem(R.id.nav_grupo_1).setTitle("HOLA");
        //!!!Se debe leer de materias.txt
        //grupo1.setTitle("PROGAVA");
        //Log.d("MAIN",grupo1.getTitle().toString());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.nav_impr) {
            Intent intent = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_SUBJECT, "Impresión de archivo");
            //intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
            intent.setData(Uri.parse("mailto:itm.papeleria@gmail.com")); // or just "mailto:" for blank
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
            startActivity(intent);
        } else if(item.getItemId()==R.id.nav_exit){
            String[] listArch=fileList();
            for(int cont=0;cont<fileList().length;cont++){
                Log.d("ARCH",listArch[cont]);
                deleteFile(listArch[cont]);
                //ELIMINAR BDD y CACHE
                SQLiteDatabase BDD = new SampleDBSQLiteHelper(this.getApplicationContext()).getWritableDatabase();
                BDD.execSQL("DELETE FROM "+DBContract.publicacion.TABLE_NAME);
                BDD.execSQL("DELETE FROM "+DBContract.comentarios.TABLE_NAME);
                BDD.execSQL("DELETE FROM "+DBContract.alumno.TABLE_NAME);
                BDD.execSQL("DELETE FROM "+DBContract.materias.TABLE_NAME);
            }
            finish();
        } else {
            displaySelectedScreen(item.getItemId());
        }
        return true;
    }


    private void displaySelectedScreen(int itemId) {

        //creating fragment object
        Fragment fragment=null;
        Bundle argumentos;
        //initializing the fragment object which is selected
        switch (itemId) {
            case R.id.nav_horario:
                //this.setContentView();
                fragment = new fragmentHorario();
                break;
            case R.id.nav_noticias:
                fragment=new fragmentNoticias();
                argumentos = new Bundle();
                argumentos.putString("url","http://antec.mx");
                argumentos.putString("titulo","Noticias");
                fragment.setArguments(argumentos);
                break;
            case R.id.nav_info:
                fragment=new informacion();
                break;
            case R.id.nav_gallery:
                fragment=new croquis();
                break;
            case R.id.nav_grupo_1:
                intentarPubl(0);
                break;
            case R.id.nav_grupo_2:
                intentarPubl(1);
                break;
            case R.id.nav_grupo_3:
                intentarPubl(2);
                break;
            case R.id.nav_grupo_4:
                intentarPubl(3);
                break;
            case R.id.nav_grupo_5:
                intentarPubl(4);
                break;
            case R.id.nav_grupo_6:
                intentarPubl(5);
                break;
            case R.id.nav_grupo_7:
                intentarPubl(6);
                break;
            case R.id.nav_grupo_8:
                intentarPubl(7);
                break;
            case R.id.ivFoto:
                fragment=new fotoGrande();
                Bundle paq1=new Bundle();
                paq1.putString("numCo",numco);
                fragment.setArguments(paq1);
                break;
            case R.id.nav_depart:
                fragment=new departFragment();
                break;
            case R.id.nav_sii:
                fragment=new fragmentNoticias();
                argumentos = new Bundle();
                argumentos.putString("url","https://sge.morelia.tecnm.mx/login/alumno");
                argumentos.putString("titulo","SGE");
                fragment.setArguments(argumentos);
                break;
        }

        //replacing the fragment
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            FrameLayout contentFrame=(FrameLayout) findViewById(R.id.content_frame);
            contentFrame.removeAllViewsInLayout();
            ft.replace(R.id.content_frame,fragment);
            ft.commit();
        }

        //DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_main);
        drawer.closeDrawer(GravityCompat.START);
    }

    public String capitalizar(String entrada){
        entrada=entrada.toLowerCase();
        String cadSalida="";
        String cadAux="";
        for(int i=0;i<entrada.length();i++){
            if(entrada.charAt(i)!=' '){
                cadAux=cadAux.concat(String.valueOf(entrada.charAt(i)));
            } else{
                cadSalida=cadSalida.concat(" "+(cadAux.substring(0,1).toUpperCase())+cadAux.substring(1));
                cadAux="";
            }
        }
        cadAux=(cadAux.substring(0,1).toUpperCase())+cadAux.substring(1);
        cadSalida=cadSalida.concat(" "+cadAux);
        return cadSalida;
    }

    void intentarPubl(int numMat){
        Intent intentoPubl=new Intent(this,grupoActivity.class);
            cursor.moveToPosition(numMat);
            intentoPubl.putExtra("codUsua",numco);
            intentoPubl.putExtra("idgrupo",cursor.getString(0));
            startActivity(intentoPubl);
    }

}
