package com.tecmor.ponyapp;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

public class croquis extends Fragment {

    public croquis() {
        // Required empty public constructor
    }


    public static croquis newInstance(String param1, String param2) {
        croquis fragment = new croquis();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_croquis, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final ImageView ImageCroquis=(ImageView) getView().findViewById(R.id.imageCroquis);
        final ImageView ImageLugar=(ImageView) getView().findViewById(R.id.marcLugar);
        final TextView Textlugar=(TextView) getView().findViewById(R.id.textLugar);
        final LinearLayout layoutVer=(LinearLayout) getView().findViewById(R.id.layoutVer);
        final FloatingActionButton flotaBtn=(FloatingActionButton) getView().getRootView().findViewById(R.id.fab);
        flotaBtn.setVisibility(View.GONE);
        ImageLugar.setVisibility(View.INVISIBLE);
        SearchView busq=(SearchView) getView().findViewById(R.id.busqueda);
        busq.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ImageLugar.setVisibility(View.INVISIBLE);
                Textlugar.setText("");
                newText=newText.toLowerCase();
                if (newText.length() > 0) {
                    Log.d("BUSQ", newText.toLowerCase());
                    //char letra = newText.toLowerCase().charAt(0);
                    //moverLugar(String.valueOf(letra));
                    if(newText.contains("edificio ")){
                        moverLugar(newText.replace("edificio ",""));
                    } else if(newText.contains("aulas de ") || newText.contains("salones de ") || newText.contains("oficinas de ")){
			            String temp=newText;
			            if(newText.contains("aulas de ")) temp=newText.replace("aulas de ","");
			            else if(newText.contains("salones de ")) temp=newText.replace("salones de ","");
			            else if(newText.contains("oficinas de ")) temp=newText.replace(" oficinas de ","");
			            switch(temp){
				            case "sistemas":
				            case "sindicato":
					            moverLugar("k");
					        break;
				            case "informática":
					            moverLugar("f");
					            break;
				            case "tics":
					            moverLugar("ag");
					            break;
				            case "contabilidad":
					            moverLugar("ñ");
					            break;
			            }
                    } else if(newText.contains("salón ") || newText.contains("salon ") || newText.contains("aula ")){
                        //Para k4, f3 y así.. excepto si es s1, s2 o s3
                        String cadTemp=newText;
                        if (newText.contains("salón ")) newText=newText.replace("salón ","");
                        else if(newText.contains("salon ")) newText=newText.replace("salon ","");
                        else if(newText.contains("aula ")) newText=newText.replace("aula ","");
                        if(newText.length()>0) {
                            if (newText.charAt(0) == 's') {
                                moverLugar(newText);
                            } else {
                                //Buscar salones como el LC y así
                                if(newText.length()>1) {
                                    if (newText.charAt(newText.length() - 1) > '0' && newText.charAt(newText.length() - 1) <= '9') {
                                        newText = newText.substring(0, newText.length() - 2);
                                        if (newText.charAt(newText.length() - 1) > '0' && newText.charAt(newText.length() - 1) <= '9') {
                                            newText = newText.substring(0, newText.length() - 2);
                                        }
                                        moverLugar(newText);
                                    }
                                }
                            }
                        }
                    } else if(newText.contains("departamento de ")){
                        newText=newText.replace("departamento de ","");
                    } else if(newText.length() < 3){
                        moverLugar(newText);
                    }
                }
                return true;
            }
        });
        getActivity().setTitle("Croquis");
    }

    private void moverLugar(String cadBusq) {
        switch (cadBusq) {
            case "a":
                ajustePos(0.394, 0.294, "Edificio A\nEdificio administrativo");
                break;
            case "b":
                ajustePos(0.446, 0.2, "Edificio B\nLaboratorio de Química analítica");
                break;
            case "c":
                ajustePos(0.518, 0.2, "Edificio C\nCoordinación de lenguas extranjeras (CLE)");
                break;
            case "ch":
                ajustePos(0.506, 0.16, "Edificio CH\nDepartamento de Ing. industrial e Ing. bioquímica");
                break;
            case "d":
                ajustePos(0.506, 0.228, "Edificio D\nLaboratorio de Ing. bioquímica");
                break;
            case "e":
                ajustePos(0.49, 0.296, "Edificio E\nAulas de Informática y taller de Dibujo");
                break;
            case "f":
                ajustePos(0.465, 0.345, "Edificio F\nAulas de TICS y sala audiovisual no. 1");
                break;
            case "g":
                ajustePos(0.54, 0.34, "Edificio G\nLaboratorios de Ing. mecánica no. 1 e Ing. eléctrica");
                break;
            case "h":
                ajustePos(0.675, 0.35, "Sección H\nUnidad deportiva y tribunas");
                break;
            case "i":
                ajustePos(0.475, 0.4, "Edificio I\nDepartamento de sistemas y cómputo");
                break;
            case "j":
                ajustePos(0.37, 0.395, "Edificio J\naulas de titulación y PADHIE");
                break;
            case "k":
                ajustePos(0.473, 0.45, "Edificio K\nAulas de Sistemas computacionales y delegación sindical");
                break;
            case "l":
                ajustePos(0.265, 0.44, "Edificio L\nAulas Licenciatura en administración");
                break;
            case "ll":
                ajustePos(0.4, 0.565, "Edificio LL\nCentro de negocios");
                break;
            case "m":
                ajustePos(0.2, 0.54, "Edificio M\nDepartamento Económico-administrativo");
                break;
            case "n":
                ajustePos(0.265, 0.54, "Edificio N\nLaboratorios de Ingeniería bioquímica");
                break;
            case "ñ":
                ajustePos(0.32, 0.505, "Edificio Ñ\nLaboratorio de Instrumentación analística");
                break;
            case "o":
                ajustePos(0.4, 0.5, "Edificio O\nLaboratorios Químico-Biológicos");
                break;
            case "p":
                ajustePos(0.35, 0.534, "Edificio P\nLaboratorio de Física");
                break;
            case "q":
                ajustePos(0.635, 0.51, "Edificio Q\nAulas de Electrónica");
                break;
            case "r":
                ajustePos(0.448, 0.574, "Edificio R\nCafetería");
                break;
            case "s":
                ajustePos(0.225, 0.575, "Edificio S\nDepartamento de recursos materiales y servicios");
                break;
            case "s1":
                ajustePos(0.245, 0.607, "Edificio S1\nConsejo estudiantil");
                break;
            case "s2":
                ajustePos(0.16, 0.62, "Edificio S2\nComedor para trabajadores");
                break;
            case "s3":
                ajustePos(0.595, 0.315, "Edificio S3\n(Laboratorio) Caldera demostrativa");
                break;
            case "t":
                ajustePos(0.29, 0.65, "Edificio T\nLaboratorio de ingeniería mecánica no. 2");
                break;
            case "u":
                ajustePos(0.38, 0.65, "Edificio U\nGimnasio auditorio");
                break;
            case "v":
                ajustePos(0.492, 0.587, "Edificio V\nDepto. de actividades extraescolares");
                break;
            case "w":
                ajustePos(0.506, 0.56, "Edificio W\nEditorial");
                break;
            case "x":
                ajustePos(0.5, 0.615, "Edificio X\nComedor estudiantil");
                break;
            case "y":
                ajustePos(0.66, 0.598, "Edificio Y\nDepartamento de Electrónica");
                break;
            case "z":
                ajustePos(0.52, 0.675, "Edificio Z\nAulas de Eléctrica");
                break;
            case "aa":
                ajustePos(0.567, 0.561, "Edificio AA\nDepto. de Ing. Eléctrica");
                break;
            case "ab":
                ajustePos(0.555, 0.635, "Edificio AB\nCubículos de Ing. Electrónica");
                break;
            case "ac":
                ajustePos(0.51, 0.683, "Edificio AC\nDepto. de Ciencias básicas");
                break;
            case "ad":
                ajustePos(0.51, 0.714, "Edificio AD\nCubículos para prof. de Ciencias básicas");
                break;
            case "ae":
                ajustePos(0.704, 0.597, "Edificio AE\nLaboratorios de Maestría en electrónica");
                break;
            case "af":
                ajustePos(0.755, 0.6, "Edificio AF\nCentro de información 600 lectores");
                break;
            case "ag":
                ajustePos(0.66, 0.186, "Edificio AG\nEduc. a dist., centro de cómputo y posgrado de Mecánica");
                break;
            case "ah":
                ajustePos(0.57, 0.7, "Edificio AH\nDepto. de Bioquímica y laboratorios");
                break;
            case "2a":
                ajustePos(0.105, 0.302, "Edificio 2A\nDepto. de Ing. Mecánica y Posg. en Materiales");
                break;
            case "2b":
                ajustePos(0.161, 0.258, "Edificio 2B\nLaboratorio de fusión al vacío");
                break;
            case "2c":
                ajustePos(0.1377, 0.2463, "Edificio 2C\nAlmacén de materias primas");
                break;
            case "2d":
                ajustePos(0.1524, 0.213, "Edificio 2D\nLaboratorio de fundición y trat. térmicos");
                break;
            case "2e":
                ajustePos(0.208, 0.208, "Edificio 2E\nAula ()");
                break;
            case "2f":
                ajustePos(0.2293, 0.141, "Edificio 2F\nDepto. de Ing. en materiales");
                break;
            case "2g":
                ajustePos(0.1783, 0.160, "Edificio 2G\nLab. de análisis químicos y fisioquímica");
                break;
            case "2h":
                ajustePos(0.1276, 0.1655, "Edificio 2H\nLaboratorio de materiales");
                break;
            case "2j":
                ajustePos(0.1273, 0.1183, "Edificio 2J\nLaboratorio de rayos X");
                break;
            case "2k":
                ajustePos(0.065, 0.075, "Edificio 2K\nAula");
                break;
            case "3a":
                ajustePos(0.77, 0.78, "Edificio 3A\nLaboratorio posgrado en Eléctrica");
                break;
            case "3b":
                ajustePos(0.82, 0.74, "Edificio 3B\nCubículos doctorado en eléctrica");
                break;
            case "e1":
                //Estacionamiento 1
                break;
            case "e2":
                //Estacionamiento 2
                break;
            case "e3":
                //Estacionamiento 3
                break;
            case "e4":
                //Estacionamiento 3
                break;
            case "e5":
                //Estacionamiento 3
                break;
            default:
                ajustePos(0, 0, "No encontrado");
                break;
        }
    }
    //  }
    //}

    float ajustePos(double x, double y, String texto){
        final ImageView ImageCroquis = (ImageView) getView().findViewById(R.id.imageCroquis);
        final ImageView ImageLugar = (ImageView) getView().findViewById(R.id.marcLugar);
        final TextView Textlugar = (TextView) getView().findViewById(R.id.textLugar);
        if(x!=0 && y!=0) ImageLugar.setVisibility(View.VISIBLE);
        ImageLugar.setX(ImageCroquis.getX() + (ImageCroquis.getWidth() * (float) x) - ImageLugar.getWidth()/2);
        ImageLugar.setY((float) (ImageCroquis.getY() + (ImageCroquis.getHeight() * (float) y) - (ImageLugar.getHeight()*0.9)));
        Textlugar.setText(texto);
        return 0;
        }

    }
