package com.tecmor.ponyapp;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.databinding.DataBindingUtil;
import android.widget.Toast;

import com.tecmor.ponyapp.databinding.ActivityGrupoBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class grupoActivity extends AppCompatActivity {

    String codGrupo;
    String codUsua;
    private ActivityGrupoBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=DataBindingUtil.setContentView(this, R.layout.activity_grupo);
        codGrupo=getIntent().getStringExtra("idgrupo");
        codUsua=getIntent().getStringExtra("codUsua");
        this.setTitle(codGrupo);
        Log.d("CREATE",codGrupo);
        binding.recycleView.setLayoutManager(new LinearLayoutManager(this));

        String[] consultaColum={"codiGrupo",DBContract.publicacion.COLUMN_CODIGO_GRUPO};
        //String[] adapterColum={DBContract.publicacion.COLUMN_USUARIO};

        SQLiteDatabase BDD=new SampleDBSQLiteHelper(this).getReadableDatabase();

        FloatingActionButton FABPubl= findViewById(R.id.FABPubl);
        FABPubl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intenNuePubl=new Intent(view.getContext(),publicacion.class);
                intenNuePubl.putExtra("codUsua",codUsua);
                intenNuePubl.putExtra("tipo",1);
                intenNuePubl.putExtra("idgrupo",codGrupo);
                startActivity(intenNuePubl);
            }
        });
        cargarRecycler();
    }

    private void leerBDD() {
        serviciosWeb sw=new serviciosWeb(getApplicationContext());
        cargarRecycler();
        try{
           if (sw.nConseguirCompaneros(codUsua) && sw.consultaPublGrupoOk(codGrupo)) {
               Log.d("Grupo: ", "Consulta ok exitosa, recargando recyclerview");
               cargarRecycler();
           }
           else{
               Log.d("SW","Error de conexión.");
               Toast.makeText(this.getApplicationContext(),"Error de conexión",Toast.LENGTH_LONG);
           }
        } catch (IOException e) {
            Toast.makeText(this.getBaseContext(),"Error de conexión; no se pudo actualizar.",Toast.LENGTH_LONG).show();
            Log.d("SW","Error al conectarse");
        e.printStackTrace();
    }
    }

    @Override
    protected void onResume() {
        super.onResume();
        leerBDD();
    };

    void cargarRecycler(){
        SQLiteDatabase BDD = new SampleDBSQLiteHelper(this).getReadableDatabase();
//        String consulta="SELECT * FROM "+DBContract.publicacion.TABLE_NAME+" ORDER BY "+DBContract.publicacion.COLUMN_CODIGO;
//        String consulta="SELECT * FROM "+DBContract.alumno.TABLE_NAME;
        String consulta=DBContract.SELECT_PUBLIC_WITH_ID + "\"" + codGrupo + "\"   ORDER BY " + DBContract.publicacion.COLUMN_FECHAPUBL + " DESC";
        Log.d("sentencia SQL: ",consulta);
        Cursor cursor = BDD.rawQuery(consulta, null);
        Log.d("CURSORCONT", String.valueOf(cursor.getCount()));
        cursor.moveToFirst();
        for(int i=0;i<cursor.getCount();i++){
            cursor.moveToPosition(i);
            String cad="";
            for(int i2=0;i2<cursor.getColumnCount();i2++){
                cad+=cursor.getColumnName(i2)+": "+cursor.getString(i2)+" ";
            }
            Log.d("CURSOR"+String.valueOf(i),cad);
        }
        Log.d("sentencia",cursor.getString(1));
       //Cursor cursor = BDD.rawQuery(DBContract.select_todas, null);
        try {
            publicacionRecyclerViewCursorAdapter adaptador=new publicacionRecyclerViewCursorAdapter(this,cursor,new JSONObject().put("numco",codUsua));
            binding.recycleView.setAdapter(adaptador);
            binding.recycleView.setLayoutManager(new SpeedyLinearLayoutManager(binding.recycleView.getContext(), SpeedyLinearLayoutManager.VERTICAL, false));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//    leerBDD();


}
