package com.tecmor.ponyapp;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.databinding.DataBindingUtil;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.tecmor.ponyapp.databinding.ActivityVerpublicacionBinding;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static android.widget.Toast.LENGTH_SHORT;

public class verpubliActivity extends AppCompatActivity {

    String codPubl;
    String codUsua;
    EditText txtComent;
    TextView txtUsuario;
    TextView txtFecha;
    TextView txtTitulo;
    TextView txtPubl;
    ImageSwitcher imgSwitch;

    private ActivityVerpublicacionBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=DataBindingUtil.setContentView(this, R.layout.activity_verpublicacion);
        codPubl=getIntent().getStringExtra("codPubl");
        codUsua=getIntent().getStringExtra("codUsua");
        //this.setTitle(codGrupo);
        this.setTitle("Publicación");
        Log.d("CREATE VER PUBLI",codPubl);
        binding.recycleView.setLayoutManager(new LinearLayoutManager(this));

        String[] consultaColum={"codiGrupo",DBContract.publicacion.COLUMN_CODIGO_GRUPO};
        //String[] adapterColum={DBContract.publicacion.COLUMN_USUARIO};
        ImageView fotoPubl=(ImageView) findViewById(R.id.FotoImage);
        fotoPubl.setImageResource(R.drawable.ic_person_black_24dp);
        txtUsuario=(TextView) findViewById(R.id.NombreLabel);
        //txtUsuario.setText("Ángel Villagómez Escobar");
        txtTitulo=(TextView) findViewById(R.id.TituloLabel);
        //tituloLbl.setText("Titulo de prueba");
        txtPubl=(TextView) findViewById(R.id.DescripLabel);
        //contenidoLbl.setText("Lorem ipsum et dolour jd nsn mouse teclado jfj ipsum et dolour jd nsn mouse teclado ipsum et dolour jd nsn mouse teclado ipsum et dolour jd nsn mouse teclado ipsum et dolour jd nsn mouse teclado");
        txtFecha=(TextView) findViewById(R.id.FechaLabel);
        txtComent=(EditText) findViewById(R.id.txtComent);
        imgSwitch=findViewById(R.id.imgSwitchVer);
        imgSwitch.setFactory(new ViewSwitcher.ViewFactory() {
            public View makeView() {
                ImageView myView = new ImageView(getApplicationContext());
                return myView;
                }
        });
        Animation in = AnimationUtils.loadAnimation(this,android.R.anim.slide_in_left);
        Animation out = AnimationUtils.loadAnimation(this,android.R.anim.slide_out_right);
        imgSwitch.setImageResource(R.mipmap.pony_grande);
        imgSwitch.setInAnimation(in);
        imgSwitch.setOutAnimation(out);
        SQLiteDatabase BDD=new SampleDBSQLiteHelper(this).getReadableDatabase();
        Cursor cursor=BDD.query(DBContract.publicacion.TABLE_NAME,
                consultaColum,
                null,
                null,
                null,
                null,
                null);
        //guardarBDD();
        //try {
            leerBDD();
        /*} finally{
            cursor.close();
        }*/

        ImageButton btnComentar=(ImageButton) findViewById(R.id.btnComentar);
        btnComentar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtComent.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(),"Escriba un comentario", LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(view.getContext(),"Publicando...",LENGTH_SHORT).show();
                    if (guardarBDD()) {
                        Toast.makeText(view.getContext(), "Publicado", LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(view.getContext(), "No se realizó la publicación", LENGTH_SHORT).show();
                    }
                }
            }
        });


        findViewById(R.id.activity_first).requestFocus();
        //SimpleCursorAdapter cursorAdapter=new SimpleCursorAdapter(this,android.R.layout.)

        //setContentView(R.layout.activity_grupo);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/
    }

    private void leerBDD(){
        SQLiteDatabase BDD=new SampleDBSQLiteHelper(this).getReadableDatabase();
        //codPubl="60";
        //String[] argumentos=new String[1];
        //argumentos[0]=codPubl;
        Cursor cursorPub=BDD.rawQuery(DBContract.SELECT_PUBLIC_WITH_CODPUBL+"\""+codPubl+"\"",null);
        //try {
            Log.d("VERPUBLI CANT ", String.valueOf(cursorPub.getColumnCount()));
            cursorPub.moveToFirst();
            for (int i = 1; i < cursorPub.getColumnCount(); i++) {
                cursorPub.moveToFirst();
                Log.d("VERPUBLI " + cursorPub.getColumnName(i), cursorPub.getString(i));
            }
            txtUsuario.setText(cursorPub.getString(4));
            txtFecha.setText(cursorPub.getString(3));
            txtTitulo.setText(cursorPub.getString(5));
            txtPubl.setText(cursorPub.getString(6));
            Cursor alumn = BDD.rawQuery("SELECT * FROM alumnos WHERE no_de_control=" + cursorPub.getString(4), null);
            //try {
                alumn.moveToFirst();
                String nombre = alumn.getString(5) + " " + alumn.getString(3) + " " + alumn.getString(4);
                txtUsuario.setText(nombre);
                serviciosWeb SW = new serviciosWeb(this);
        try {
            if (!SW.consultaComenOk(codPubl)) {
                //Toast.makeText(this.getApplicationContext(), "No se pudo conectar", Toast.LENGTH_SHORT).show();
                Log.d("ERROR SERV", "ERROR EN VERPUBLIC");
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Cursor cursor = BDD.rawQuery(DBContract.SELECT_COMENT_WITH_CODPUBL + "'" + codPubl + "' ORDER BY " + DBContract.comentarios.COLUMN_FECHACOM + " DESC", null);
                //try {
        try {
            binding.recycleView.setAdapter(new comentariosRecyclerViewCursorAdapter(this, cursor,new JSONObject().put("numco",codUsua)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
                /*}finally {
                    cursor.close();
                }*/
            /*} finally {
                alumn.close();
            }*/
        /*} finally {
            cursorPub.close();
        }*/
                //}
            }

    private boolean guardarBDD(){
        boolean exito=false;
        String respuesta;
        serviciosWeb SW=new serviciosWeb(this);
        JSONObject jsonPost=new JSONObject();
        JSONObject jsonRespuesta = null;
        try {
            jsonPost.put("usuario",codUsua);
            jsonPost.put("codPubl",codPubl);
            jsonPost.put("comentario",txtComent.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("comenJSON",jsonPost.toString());
        try {
            return SW.subirComent(jsonPost.toString());
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        leerBDD();
    };
}