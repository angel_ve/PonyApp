package com.tecmor.ponyapp;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class fragmentNoticias extends Fragment {

    private String sitioWeb="";
    private String titulo="";

    public fragmentNoticias() {
        // Required empty public constructor
    }


    public static fragmentNoticias newInstance(String param1, String param2) {
        fragmentNoticias fragment = new fragmentNoticias();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            sitioWeb=getArguments().getString("url");
            titulo=getArguments().getString("titulo");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_noticias, container, false);
    }

    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle(titulo);
        final FloatingActionButton flotaBtn=(FloatingActionButton) getView().getRootView().findViewById(R.id.fab);
        flotaBtn.setVisibility(View.GONE);
        final WebView myWebView = (WebView) getView().findViewById(R.id.webview);
        final TextView statusInt=(TextView) getView().findViewById(R.id.conInt);
        final Button btnRein=(Button) getView().findViewById(R.id.btnRein);
        final LinearLayout layoutLinear=(LinearLayout) getView().findViewById(R.id.layoutRei);
        layoutLinear.setVisibility(View.INVISIBLE);
        statusInt.setVisibility(View.INVISIBLE);
        btnRein.setVisibility(View.INVISIBLE);
        myWebView.setVisibility(View.VISIBLE);
        //myWebView.clearCache(true);
        myWebView.loadUrl(sitioWeb);
        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon){
                layoutLinear.setVisibility(View.GONE);
                statusInt.setVisibility(View.GONE);
                btnRein.setVisibility(View.GONE);
                if(url.equals("about:blank") == false) {
                    myWebView.setVisibility(View.VISIBLE);
                }
            }
            public void onReceivedError(WebView view, int errorCode,
                                        String description, String failingUrl) {

                myWebView.setVisibility(View.INVISIBLE);
                layoutLinear.setVisibility(View.VISIBLE);
                statusInt.setVisibility(View.VISIBLE);
                btnRein.setVisibility(View.VISIBLE);
            }
        });
        btnRein.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                myWebView.loadUrl("about:blank");
                myWebView.clearHistory();

                myWebView.loadUrl(sitioWeb);
                return false;
            }
        });
        WebSettings mbs = myWebView.getSettings();
        mbs.setJavaScriptEnabled(true);
    }

}
