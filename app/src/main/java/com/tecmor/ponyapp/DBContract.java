package com.tecmor.ponyapp;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

public final class DBContract {

    //public static final String SELECT_PUBLIC_WITH_ID="SELECT * FROM "+publicacion.TABLE_NAME+" WHERE "+publicacion.COLUMN_CODIGO_GRUPO+"=?";
    //public static final String SELECT_PUBLIC_WITH_ID="SELECT rowid,* FROM "+publicacion.TABLE_NAME+" INNER JOIN "+"usuarios"+" ON "+publicacion.TABLE_NAME+"."+publicacion.COLUMN_USUARIO+"="+usuario.COLUMN_CODIGO+" WHERE "+publicacion.COLUMN_CODIGO_GRUPO+"=";
    //public static final String SELECT_PUBLIC_WITH_ID="SELECT rowid,* FROM "+publicacion.TABLE_NAME+" INNER JOWHERE "+publicacion.COLUMN_CODIGO_GRUPO+"=";
    public static final String SELECT_PUBLIC_WITH_CODPUBL="SELECT * FROM "+publicacion.TABLE_NAME+" WHERE "+publicacion.COLUMN_CODIGO+"=";
    //public static final String SELECT_PUBLIC_WITH_ID="SELECT * FROM "+publicacion.TABLE_NAME+" WHERE "+publicacion.COLUMN_CODIGO_GRUPO+"=";
    public static final String select_todas="SELECT * FROM publicaciones";
    public static final String SELECT_PUBLIC_WITH_ID="SELECT * FROM "+publicacion.TABLE_NAME+" INNER JOIN "+alumno.TABLE_NAME+" ON "+publicacion.TABLE_NAME+"."+publicacion.COLUMN_USUARIO+"="+alumno.TABLE_NAME+'.'+alumno.COLUMN_NODECONTROL+" WHERE "+publicacion.TABLE_NAME+"."+publicacion.COLUMN_ACTIVO+"=1 AND "+publicacion.TABLE_NAME+"."+publicacion.COLUMN_CODIGO_GRUPO+"=";
    public static final String SELECT_COMENT_WITH_CODPUBL="SELECT * FROM "+comentarios.TABLE_NAME+" INNER JOIN "+alumno.TABLE_NAME+" ON "+comentarios.COLUMN_USUA+"="+alumno.COLUMN_NODECONTROL+" WHERE "+comentarios.TABLE_NAME+"."+comentarios.COLUMN_ACTIVO+"=1 AND "+comentarios.TABLE_NAME+"."+comentarios.COLUMN_CODPUBL+"=";
    //public static final String SELECT_COMENT_WITH_CODPUBL="SELECT * FROM "+comentarios.TABLE_NAME+" WHERE "+comentarios.COLUMN_CODPUBL+"=";
    public static final String SELECT_COMENT_WITH_CODCOMENT="SELECT * FROM "+comentarios.TABLE_NAME+" WHERE "+comentarios.COLUMN_CODIGO+"=";
    public static final String DELETE_FROM_PUBLIC_WITH_CODGRU="DELETE FROM "+publicacion.TABLE_NAME+" WHERE "+publicacion.COLUMN_CODIGO_GRUPO+"=\"";
    public static final String DELETE_FROM_COMENT_WITH_CODPUBL="DELETE FROM "+comentarios.TABLE_NAME+" WHERE "+comentarios.COLUMN_CODPUBL+"=\"";
    public static final String SELECT_FROM_ALUMNOS="SELECT * FROM "+alumno.TABLE_NAME;
    public static final String SELECT_FROM_PUBLICACION="SELECT * FROM "+publicacion.TABLE_NAME;
    //public static final String SELECT_PUBL_WITH_CODGRU="SELECT * FROM "+publicacion.TABLE_NAME+" WHERE "+publicacion.COLUMN_CODIGO_GRUPO+"=";
    public static final String SELECT_FROM_MATERIAS="SELECT * FROM "+materias.TABLE_NAME;

    private DBContract() {
    }

    public static class publicacion implements BaseColumns {
        public static final String TABLE_NAME = "publicaciones";
        public static final String COLUMN_CODIGO = "codigo";
        public static final String COLUMN_CODIGO_GRUPO = "codiGrupo";
        public static final String COLUMN_FECHAPUBL = "fechaPubl";
        public static final String COLUMN_TITULO = "titulo";
        public static final String COLUMN_USUARIO = "usuario";
        public static final String COLUMN_ACTIVO="valido";
        public static final String COLUMN_CONTENIDO = "contenido";

        public static final String CREATE_TABLE = "CREATE TABLE "+TABLE_NAME+" ("+COLUMN_CODIGO+" INTEGER NOT NULL, "+COLUMN_CODIGO_GRUPO+" VARCHAR(6), "+COLUMN_ACTIVO+" INTEGER NOT NULL DEFAULT 1, "+COLUMN_FECHAPUBL+" TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL, "+COLUMN_USUARIO+" INTEGER, "+COLUMN_TITULO+" VARCHAR(40), "+COLUMN_CONTENIDO+" VARCHAR(200), PRIMARY KEY("+COLUMN_CODIGO+"))";
        public static final String DESCRIBE_TABLE="PRAGMA table_info([publicaciones])";
    }

    public static class alumno implements BaseColumns{
        public static final String TABLE_NAME="alumnos";
        public static final String COLUMN_NODECONTROL="no_de_control";
        public static final String COLUMN_CARRERA="carrera";
        public static final String COLUMN_ESPECIALIDAD="especialidad";
        public static final String COLUMN_SEMESTRE="semestre";
        public static final String COLUMN_APELLIDO_PATERNO="apellido_paterno";
        public static final String COLUMN_APELLIDO_MATERNO="apellido_materno";
        public static final String COLUMN_NOMBRE_ALUMNO="nombre_alumno";
        public static final String CREATE_TABLE="CREATE TABLE "+TABLE_NAME+" ("+COLUMN_NODECONTROL+" INTEGER NOT NULL, "+COLUMN_CARRERA+" INTEGER NOT NULL, "+COLUMN_ESPECIALIDAD+" VARCHAR(5), "+COLUMN_APELLIDO_PATERNO+" VARCHAR(15), "+COLUMN_APELLIDO_MATERNO+" VARCHAR(15), "+COLUMN_NOMBRE_ALUMNO+" VARCHAR(20), "+COLUMN_SEMESTRE+" INTEGER NOT NULL, PRIMARY KEY("+COLUMN_NODECONTROL+"))";
    }

    public static class comentarios implements BaseColumns{
        public static final String TABLE_NAME="comentarios";
        public static final String COLUMN_USUA="usuario";
        public static final String COLUMN_COMENTARIO="comentario";
        public static final String COLUMN_CODIGO="codCom";
        public static final String COLUMN_CODPUBL="codPubl";
        public static final String COLUMN_FECHACOM="fechaCom";
        public static final String COLUMN_ACTIVO="valido";
        public static final String CREATE_TABLE="CREATE TABLE comentarios (codCom INTEGER, valido INTEGER NOT NULL DEFAULT 1, codPubl INTEGER, usuario VARCHAR(15), comentario VARCHAR(150), fechaCom TIMESTAMP)";
    }

    public static class materias implements BaseColumns{
        public static final String TABLE_NAME="materias";
        public static final String COLUMN_MATERIA="materia";
        public static final String COLUMN_NOMBRE_COMPLETO="nombre_completo_materia";
        public static final String COLUMN_NOMBRE_ABREVIADO="nombre_abreviado_materia";
        public static final String CREATE_TABLE="CREATE TABLE "+TABLE_NAME+" ("+COLUMN_MATERIA+" VARCHAR(5) PRIMARY KEY, "+COLUMN_NOMBRE_COMPLETO+" VARCHAR(80), "+COLUMN_NOMBRE_ABREVIADO+" VARCHAR(35))";
//        public static final String alumno="no_de_control";
//        public static final String codMat="materia";
//        public static final String grupo="grupo";
//        public static final String periodo="periodo";
//        public static final String calificacion="calificacion";
//        public static final String tipo_evaluacion="tipo_evaluacion";
//        public static final String repeticion="repeticion";
//        public static final String no_presento=
    }
}

