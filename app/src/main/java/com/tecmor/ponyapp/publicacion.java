package com.tecmor.ponyapp;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Layout;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import static android.widget.Toast.LENGTH_SHORT;

public class publicacion extends AppCompatActivity {

    String codGrupo;
    int FILE_SELECT_CODE;
    ListView listViewArch;
    ArrayList<String> arrayArch = new ArrayList<String>();
    EditText textComentario;
    EditText textTitulo;
    String codUsua;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publicacion);
        textTitulo=(EditText) findViewById(R.id.txtPublTitu);
        textComentario=(EditText) findViewById(R.id.txtPublConte);
        this.setTitleColor(R.color.colorWhite);
        listViewArch=(ListView) findViewById(R.id.lvArch);
        codGrupo=getIntent().getStringExtra("idgrupo");
        codUsua=getIntent().getStringExtra("codUsua");
        Log.d("Publicacion","codgrupo: "+codGrupo);
        int tipo = getIntent().getIntExtra("tipo", 0); //1 para publicación, 2 para comentario
        if (tipo == 1)
            this.setTitle("Nueva publicación");
        else if (tipo == 2){
            this.setTitle("Nuevo comentario");
            textTitulo.setVisibility(View.GONE);
            textComentario.setHint("Contenido del comentario");
        }
        ImageButton adjBtn=(ImageButton) findViewById(R.id.btnAdjuntar);
        adjBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int permisos=ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission_group.STORAGE);
                if(permisos!= PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(
                            (Activity) view.getContext(),
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    permisos);
                }
                Intent intentoAdj=new Intent(Intent.ACTION_GET_CONTENT);
                //intentoAdj.setType("text/* image/* application/pdf");
                /*Esto funcionaba
                intentoAdj.setType("text/*|application/pdf|image/*");
                intentoAdj.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"application/pdf","application/msword","application/","image/*","text/*"});
                */
                intentoAdj.setType("text/*|application/*|image/*");
                intentoAdj.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"application/*","image/*","text/*"});
                startActivityForResult(Intent.createChooser(intentoAdj, "Select a File to Upload"),FILE_SELECT_CODE);
            }
        });

        Button btnPubl=(Button) findViewById(R.id.btnPublicar);
        btnPubl.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //CHECAR QUE NO ESTÉ VACÍO
                if (textTitulo.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(),"Escriba un título para la publicación", LENGTH_SHORT).show();
                }
                else if(textComentario.getText().toString().isEmpty()){
                    Toast.makeText(getApplicationContext(),"Escriba contenido de la publicación", LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(view.getContext(),"Publicando...",LENGTH_SHORT).show();
                    if(guardarBDD()){
                        Toast.makeText(getBaseContext(),"Publicación realizada",Toast.LENGTH_SHORT).show();
                    finish();}
                    else{
                        Toast.makeText(getApplicationContext(),"Error: no se pudo subir la publicación.",Toast.LENGTH_SHORT).show();
                    }
                }
                    /*else {
                    Toast.makeText(view.getContext(),"Publicando...",LENGTH_SHORT).show();
                    if (guardarBDD()) {
                        Toast.makeText(view.getContext(), "Publicado", LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(view.getContext(), "No se realizó la publicación", LENGTH_SHORT).show();
                    }
                }*/
            }
        });
    }

    private boolean guardarBDD(){
        serviciosWeb SW=new serviciosWeb(this);
        JSONObject jsonPost=new JSONObject();
        try {
            jsonPost.put(DBContract.publicacion.COLUMN_USUARIO,codUsua);
            jsonPost.put(DBContract.publicacion.COLUMN_CONTENIDO,textComentario.getText().toString());
            jsonPost.put(DBContract.publicacion.COLUMN_TITULO,textTitulo.getText().toString());
            jsonPost.put(DBContract.publicacion.COLUMN_CODIGO_GRUPO,codGrupo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("publJSON",jsonPost.toString());
        try {
            return SW.subirPubl(jsonPost.toString());
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_SELECT_CODE) {
            if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    arrayArch.add(String.valueOf(uri));
                    Log.d("Archivo", String.valueOf(uri));
                listViewArch.setAdapter(new ArrayAdapter<String> (this, R.layout.publicacion_item,arrayArch));
                //listViewArch.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,arrayArch));
            }
        }
    }

}
