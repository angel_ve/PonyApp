package com.tecmor.ponyapp;
import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.tecmor.ponyapp.departFragment.OnListFragmentInteractionListener;
import com.tecmor.ponyapp.dummy.DummyContent.DummyItem;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MydepartRecyclerViewAdapter extends RecyclerView.Adapter<MydepartRecyclerViewAdapter.ViewHolder> {

    private final List<DummyItem> mValues;
    private final OnListFragmentInteractionListener mListener;
    private final Activity activityP;

    public MydepartRecyclerViewAdapter(List<DummyItem> items, OnListFragmentInteractionListener listener, Activity activity) {
        mValues = items;
        mListener = listener;
        activityP=activity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_depart, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mNombreDept.setText("Departamento A");
        holder.mHorasDept.setText("08:00-14:00");
        holder.mUbicaDept.setText("Norte del Tec. (ver en el mapa)");
        holder.mDescDept.setText("Departamento principal\nServicios:\n-Dirección\nCoord. Académica");
        holder.mLlamar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:"));

                if (ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    int MY_PERMISSIONS_REQUEST_CALL_PHONE=8;
                    ActivityCompat.requestPermissions(activityP,
                            new String[]{Manifest.permission.CALL_PHONE},
                            MY_PERMISSIONS_REQUEST_CALL_PHONE);
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return false;
                }
                view.getContext().startActivity(callIntent);
                return false;
            }
        });

        holder.mEmail.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Intent intentMail = new Intent(Intent.ACTION_SENDTO); // it's not ACTION_SEND
                intentMail.setType("text/plain");
                //intent.putExtra(Intent.EXTRA_TEXT, "Body of email");
                intentMail.setData(Uri.parse("mailto:edificioa@gmail.com")); // or just "mailto:" for blank
                intentMail.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // this will make such that when user returns to your app, your app is displayed, instead of the email app.
                view.getContext().startActivity(intentMail);
                return false;
            }
        });
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final ImageView mDepart;
        public final TextView mNombreDept;
        public final TextView mHorasDept;
        public final TextView mUbicaDept;
        public final TextView mDescDept;
        public final Button mLlamar;
        public final Button mEmail;
        public DummyItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mDepart=(ImageView) view.findViewById(R.id.ivDepar);
            mNombreDept=(TextView) view.findViewById(R.id.txtNombreDep);
            mHorasDept=(TextView) view.findViewById(R.id.txtHorasDep);
            mUbicaDept=(TextView) view.findViewById(R.id.txtUbicDep);
            mDescDept=(TextView) view.findViewById(R.id.txtDescripDept);
            mLlamar=(Button) view.findViewById(R.id.btnLlamarDep);
            mEmail=(Button) view.findViewById(R.id.btnMailDep);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNombreDept.getText() + "'";
        }
    }

}
